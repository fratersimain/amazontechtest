﻿using AmazonTechTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using AmazonTechTest.Utilities.StringUtilities;
using AmazonTechTest.Utilities.FileUtilities;

namespace AmazonTechTest.Console
{
    class Program
    {
        #region Static Main
        static void Main(string[] args)
        {
            ///Init dependencies
            IStringUtilities stringUtilities = new StringUtilities();
            IFileUtilities fileUtilities = new FileUtilities();

            string fleetStateFilePath = ConfigurationManager.AppSettings["FleetStatePath"];
            string statisticsResultFilePath = ConfigurationManager.AppSettings["StatsOutputPath"];
            string errorLogPath = ConfigurationManager.AppSettings["ErrorLogPath"];

            ///Instantiate a new Host Fleet Service to calculate the Summary Stats
            IHostFleetService hostFleetService = new HostFleetService();
            HostFleetSummaryStatistics summaryStatistics = hostFleetService.CalculateSummaryStatistics();

            ///Show results on console and write to a new file
            using (System.IO.StreamWriter file = fileUtilities.GetStreamWriter(statisticsResultFilePath, false))
            {
                ///Init outPut service to write to consol and output file
                IOutPutService outPutService = new OutPutService(file);

                ///Check for null object indicating no file to read
                if (summaryStatistics == null)
                {
                    ///Show error message
                    outPutService.WriteToOutPut("No FleetState.txt file found");

                    ///Pause to show result
                    System.Console.ReadKey();

                    ///The End 
                    return;
                }

                ///Empty slots for each instance
                outPutService.WriteToOutPut("EMPTY:");
                foreach (KeyValuePair<string, int> instanceSlots in summaryStatistics.InstanceEmptySlots)
                {
                    outPutService.WriteToOutPut(string.Format("{0}={1};", instanceSlots.Key, instanceSlots.Value));
                }
                outPutService.WriteLineToOutPut(string.Empty);

                ///Full slots for each instance
                outPutService.WriteToOutPut("FULL:");
                foreach (KeyValuePair<string, int> instanceSlots in summaryStatistics.InstanceFullSlots)
                {
                    outPutService.WriteToOutPut(string.Format("{0}={1};", instanceSlots.Key, instanceSlots.Value));
                }
                outPutService.WriteLineToOutPut(string.Empty);

                ///Most filled Instance Types
                outPutService.WriteToOutPut("MOST FILLED:");
                foreach (KeyValuePair<string, HostSlotAllocation> instanceSlots in summaryStatistics.InstanceMostFullSlots)
                {
                    outPutService.WriteToOutPut(string.Format("{0}={1},{2};", instanceSlots.Key, instanceSlots.Value.FullSlots, instanceSlots.Value.EmptySlots));
                }
                outPutService.WriteLineToOutPut("");
                outPutService.WriteLineToOutPut("");

                ///Display error warning
                if (summaryStatistics.CheckErrorLog)
                {
                    outPutService.WriteLineToOutPut("Errors were present in the original data.");
                    outPutService.WriteLineToOutPut("Please check the error log:");
                    outPutService.WriteLineToOutPut(stringUtilities.RemoveEscapeCharacters(errorLogPath));
                }

                ///Display results file location
                outPutService.WriteLineToOutPut("");
                outPutService.WriteLineToOutPut("The output file is in this location:");
                outPutService.WriteLineToOutPut(stringUtilities.RemoveEscapeCharacters(statisticsResultFilePath));

                ///Display time of test completed
                outPutService.WriteLineToOutPut("");
                outPutService.WriteLineToOutPut(string.Format("Test completed at: {0}", DateTime.Now.ToString()));
            }

            ///Pause to show result
            System.Console.ReadKey();

            ///The end
        }
        #endregion
    }
}
