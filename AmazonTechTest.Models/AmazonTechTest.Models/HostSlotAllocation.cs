﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Models
{
    public class HostSlotAllocation
    {
        #region Public Properties
        public int FullSlots { get; set; }
        public int EmptySlots { get; set; }
        #endregion

    }
}
