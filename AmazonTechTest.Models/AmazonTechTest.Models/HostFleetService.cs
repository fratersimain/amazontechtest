﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmazonTechTest.Utilities.FileUtilities;
using AmazonTechTest.Models;
using System.IO;

namespace AmazonTechTest.Models
{
    public interface IHostFleetService
    {
        HostFleetSummaryStatistics CalculateSummaryStatistics();
    }

    public class HostFleetService : IHostFleetService
    {
        #region Private Fields
        ///Init dependencies (use IOC)
        IFileUtilities _fileUtilities = new FileUtilities();
        IErrorLog _errorLog = new ErrorLog();

        /// Get file locations from config file
        readonly string _fleetStatePath = System.Configuration.ConfigurationManager.AppSettings["FleetStatePath"];
        readonly string _errorLogPath = System.Configuration.ConfigurationManager.AppSettings["errorLogLocation"];
        readonly string _statsOutputPath = System.Configuration.ConfigurationManager.AppSettings["statsOutputLocation"];
        #endregion

        #region Private Object Methods
        private bool DoesThisHostHaveTheMostFullSlotsForThisInstanceType(Dictionary<string, HostSlotAllocation> allInstanceMostFullSlots, Host host, int numberOfFullSlots)
        {
            return (allInstanceMostFullSlots[host.InstanceType].FullSlots < numberOfFullSlots);
        }

        private bool DoesInstanceTypeAlreadyExistInMostFullSlotsDictionary(Dictionary<string, HostSlotAllocation> allInstanceMostFullSlots, Host host)
        {
            return allInstanceMostFullSlots.ContainsKey(host.InstanceType);
        }

        private void AddRunningTotalToDictionary(Dictionary<string, int> dictionary, string key, int value)
        {
            ///Increment the empty slots dictionary
            if (dictionary.ContainsKey(key))
            {
                ///Add empty slots to existing dictionary entry
                dictionary[key] += value;
            }
            else
            {
                ///Create new dictionary entry
                dictionary[key] = value;
            }
        }

        private FleetStateDictionary GetFleetStateDictionary(string[] fleetState)
        {
            ///Create dictionary<HostID,Host>
            Dictionary<string, Host> hostDictionary = new Dictionary<string, Host>();
            bool isError = false;
            int forEachCounter = 0;

            foreach (string line in fleetState)
            {
                ///Increment line counter
                forEachCounter++;

                if (string.IsNullOrEmpty(line))
                {
                    ///Do nothing
                    continue;
                }

                ///Get the host
                Host host = Host.DeserializeLineFeed(line);

                ///Check if host exists 
                if (host == null)
                {
                    ///write to error log
                    _errorLog.WriteToLog(forEachCounter,line, "Cannot deserialize line");
                    isError = true;
                    continue;
                }

                ///Check if host already exists in Stats
                if (hostDictionary.ContainsKey(host.ID))
                {
                    ///write to error log
                    _errorLog.WriteToLog(forEachCounter, line, "Duplicate");
                    isError = true;
                    continue;
                }

                ///Add to dictionary
                hostDictionary.Add(host.ID, host);

            }

            ///Package result
            FleetStateDictionary result = new FleetStateDictionary
            {
                AllHost = hostDictionary,
                ErrorsPresent = isError
            };

            return result;
        }
        #endregion

        #region Public Object Methods
        public HostFleetSummaryStatistics CalculateSummaryStatistics()
        {
            ///Create array of all hosts from FleetState file
            string[] fleetStateArray;

            ///Read file
            if (_fileUtilities.FileExists(_fleetStatePath))
            {
                fleetStateArray = _fileUtilities.ReadFileToArray(_fleetStatePath);
            }else
            {
                ///No file found return null object
                return null;
            }

            ///Load dictionary
            FleetStateDictionary fleetStateDictionary = GetFleetStateDictionary(fleetStateArray);

            ///Declare dictionary<instance type, number Of Empty slots>
            Dictionary<string, int> allInstanceTypeEmptySlots = new Dictionary<string, int>();

            ///Declare dictionary<instance type, number Of full slots>
            Dictionary<string, int> allInstanceTypeFullSlots = new Dictionary<string, int>();

            ///Declare dictionary<instance type, number Of full slots>
            Dictionary<string, HostSlotAllocation> allInstanceMostFullSlots = new Dictionary<string, HostSlotAllocation>();

            foreach (KeyValuePair<string, Host> fleetState in fleetStateDictionary.AllHost)
            {
                ///Get host object
                Host host = fleetState.Value;

                ///Get number of empty and full slots in host object
                int numberOfEmptySlots = host.GetNumberOfEmptySlots();
                int numberOfFullSlots = host.GetNumberOfFullSlots();

                ///Add running totals to empty and full dictionaries
                AddRunningTotalToDictionary(allInstanceTypeEmptySlots, host.InstanceType, numberOfEmptySlots);

                AddRunningTotalToDictionary(allInstanceTypeFullSlots, host.InstanceType, numberOfFullSlots);

                ///Identify and add most filled host to dictionary of most filled per instance 
                if ((!DoesInstanceTypeAlreadyExistInMostFullSlotsDictionary(allInstanceMostFullSlots, host)) ||
                    (DoesInstanceTypeAlreadyExistInMostFullSlotsDictionary(allInstanceMostFullSlots, host) &&
DoesThisHostHaveTheMostFullSlotsForThisInstanceType(allInstanceMostFullSlots, host, numberOfFullSlots))
                    )
                {
                    allInstanceMostFullSlots[host.InstanceType] = new HostSlotAllocation
                    {
                        FullSlots = numberOfFullSlots,
                        EmptySlots = numberOfEmptySlots
                    };
                }
            }

            ///Sort the dictionaries
            allInstanceTypeEmptySlots = allInstanceTypeEmptySlots.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);
            allInstanceTypeFullSlots = allInstanceTypeFullSlots.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);
            allInstanceMostFullSlots = allInstanceMostFullSlots.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            ///Package results
            HostFleetSummaryStatistics result = new HostFleetSummaryStatistics
            {
                CheckErrorLog = fleetStateDictionary.ErrorsPresent,
                InstanceEmptySlots = allInstanceTypeEmptySlots,
                InstanceFullSlots = allInstanceTypeFullSlots,
                InstanceMostFullSlots = allInstanceMostFullSlots
            };

            return result;

        }
        #endregion
    }
}
