﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Models
{
    class FleetStateDictionary
    {
        #region Public Properties
        public Dictionary<string, Host> AllHost;
        public bool ErrorsPresent { get; set; }
        #endregion
    }
}
