﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Models
{
    public interface IOutPutService
    {
        void WriteToOutPut(string outPut);

        void WriteLineToOutPut(string outPut);
    }

    public class OutPutService : IOutPutService
    {
        #region Private Fields
        System.IO.StreamWriter _file;
        #endregion

        #region Public Object Methods

        public OutPutService(System.IO.StreamWriter file)
        {
            _file = file;
        }

        public void WriteToOutPut(string outPut)
        {
            System.Console.Write(outPut);
            _file.Write(outPut);
        }

        public void WriteLineToOutPut(string outPut)
        {
            System.Console.WriteLine(outPut);
            _file.WriteLine(outPut);
        }

        #endregion
    }
}
