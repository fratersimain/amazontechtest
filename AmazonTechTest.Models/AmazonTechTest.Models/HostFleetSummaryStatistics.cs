﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Models
{
    public class HostFleetSummaryStatistics
    {
        #region Public Properties

        public Dictionary<string, int> InstanceEmptySlots { get; set; }

        public bool CheckErrorLog { get; set; }

        public Dictionary<string, HostSlotAllocation> InstanceMostFullSlots { get; set; }

        public Dictionary<string, int> InstanceFullSlots { get; set; }

        #endregion
    }
}
