﻿using AmazonTechTest.Utilities.FileUtilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Models
{
    interface IErrorLog
    {
        void WriteToLog(int lineNumber, string information, string cause);
    }

    class ErrorLog : IErrorLog
    {
        #region Private Fields
         /// Init dependencies
        readonly string _errorLogPath = System.Configuration.ConfigurationManager.AppSettings["ErrorLogPath"];

        IFileUtilities _fileUtilities = new FileUtilities();

        #endregion

        #region Constructor
        public ErrorLog()
        {
            ///Create folder and Delete Existing File
            string errorLogPath = Path.GetDirectoryName(_errorLogPath);

            if (!_fileUtilities.DirectoryExists(errorLogPath))
            {
                ///Create path to error log
                _fileUtilities.CreateDirectory(errorLogPath);
            }
            else
            {
                ///Delete existing error log
                if (_fileUtilities.FileExists(_errorLogPath))
                {
                    _fileUtilities.FileDelete(_errorLogPath);
                }
            }
        }

        #endregion

        #region Public Object Methods
           public void WriteToLog(int lineNumber, string data, string cause)
        {
            ///Append to file
            using (System.IO.StreamWriter file = _fileUtilities.GetStreamWriter(_errorLogPath, true) )
            {
                file.WriteLine("Line Number");
                file.WriteLine(lineNumber);
                file.WriteLine("Data");
                file.WriteLine(data);
                file.WriteLine("Cause");
                file.WriteLine(cause);
                file.WriteLine(DateTime.Now.ToString());
                file.WriteLine("");
            }
        }
        #endregion
     
    }
}
