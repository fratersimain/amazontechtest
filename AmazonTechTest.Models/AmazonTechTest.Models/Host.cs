﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Models
{
    public class Host
    {
        #region Public Properties
        public string ID { get; set; }
        public string InstanceType { get; set; }
        public string NumberOfSlots { get; set; }
        public List<bool> Slots { get; set; }
        #endregion

        #region Constructor
        public Host()
        {
            Slots = new List<bool>();
        }


        #endregion

        #region Private Static Methods
        private static bool IsLineNullOrEmpty(string line)
        {
            return string.IsNullOrEmpty(line);
        }

        private static bool HasCorrectNumberOfFields(string[] lineValues)
        {
            bool isValid = true;

            ///Check the line have the meta fields
            int numberOfMetaFields = 3;
            if (lineValues.Length < numberOfMetaFields)
            {
                isValid = false;
            }

            ///parse the number of slots field
            int numberOfSlots = 0;
            if (isValid)
            {
                ///Get number of slot fields
                int indexOfNumberOfSlots = 2;
                if (!int.TryParse(lineValues[indexOfNumberOfSlots], out numberOfSlots))
                {
                    ///Cound not parse the number of slots
                    isValid = false;
                }
            }

            ///Check if has the correct number of fields for the number of slots
            if (isValid)
            {
                if (lineValues.Length != numberOfMetaFields + numberOfSlots)
                {
                    isValid = false;
                }
            }

            return isValid;
        }
        #endregion

        #region Public Static Methods
        public static Host DeserializeLineFeed(string line)
        {
            Host result = new Host();
            bool isError = false;

            try
            {
                ///Split by comma delimiter
                string[] lineValues = line.Split(',');

                ///Validate lineValues
                if (IsLineNullOrEmpty(line))
                {
                    isError = true;
                }

                if (!isError &&
                    !HasCorrectNumberOfFields(lineValues))
                {
                    isError = true;
                }

                ///Set host properties
                if (!isError)
                {
                    result.ID = lineValues[0];
                    result.InstanceType = lineValues[1];
                    result.NumberOfSlots = lineValues[2];


                    ///Set slots
                    for (int i = 3; i <= lineValues.Length - 1; i++)
                    {
                        bool isSlotFull = Convert.ToBoolean(Convert.ToInt16(lineValues[i]));
                        result.Slots.Add(isSlotFull);
                    }

                }

            }
            catch (Exception ex)
            {
                isError = true;
            }

            ///Return result
            if (isError)
            {
                return null;
            }
            else
            {
                return result;
            }
        }
        #endregion

        #region Public Object Methods
        internal int GetNumberOfEmptySlots()
        {
            int result = 0;

            foreach (bool isSlotFull in Slots)
            {
                if (!isSlotFull)
                {
                    result++;
                }
            }

            return result;
        }

        internal int GetNumberOfFullSlots()
        {
            int result = 0;

            foreach (bool isSlotFull in Slots)
            {
                if (isSlotFull)
                {
                    result++;
                }
            }

            return result;
        }

        #endregion

    }
}
