﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Utilities.StringUtilities
{
    public interface IStringUtilities
    {
        string RemoveEscapeCharacters(string originalString);
    }

    public class StringUtilities : IStringUtilities
   {
       #region Public Object Methods
        public string RemoveEscapeCharacters(string originalString)
        {
            string result = originalString.Replace("\\\\", @"\");
                return result;
        }
       #endregion
      
    }
}
