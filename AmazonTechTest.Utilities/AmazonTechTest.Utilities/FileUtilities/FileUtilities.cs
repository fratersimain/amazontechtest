﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonTechTest.Utilities.FileUtilities
{
    public interface IFileUtilities
    {
        string[] ReadFileToArray(string filePath);

        bool FileExists(string filePath);

        bool DirectoryExists(string filePath);

        void CreateDirectory(string filePath);

        void FileDelete(string filePath);

        StreamWriter GetStreamWriter(string filePath, bool isAppendToFile);
    }

    public class FileUtilities : IFileUtilities
    {
        #region Public Object Methods
        public string[] ReadFileToArray(string filePath)
        {
            string[] result = File.ReadAllLines(filePath);

            return result;
        }

        public bool FileExists(string filePath)
        {
            bool result = File.Exists(filePath);
            return result;
        }

        public bool DirectoryExists(string filePath)
        {
            bool result = Directory.Exists(filePath);
            return result;
        }

        public void CreateDirectory(string filePath)
        {
            Directory.CreateDirectory(filePath);
        }

        public void FileDelete(string filePath)
        {
            File.Delete(filePath);
        }

        public StreamWriter GetStreamWriter(string filePath, bool isAppendToFile)
        {
            StreamWriter result = new System.IO.StreamWriter(filePath, isAppendToFile);
            return result;
        }
        #endregion
    }
}
